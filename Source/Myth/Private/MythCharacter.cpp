// Fill out your copyright notice in the Description page of Project Settings.


#include "MythCharacter.h"

// Sets default values
AMythCharacter::AMythCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMythCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMythCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//Get Actor view point from character head socket
void AMythCharacter::GetActorEyesViewPoint(FVector& Location, FRotator& Rotation) const
{
	Location = GetMesh()->GetSocketLocation("EyesSocket");
	Rotation = GetActorRotation();
	Rotation.Yaw = GetMesh()->GetSocketTransform("EyesSocket").Rotator().Yaw;
}

// Called to bind functionality to input
void AMythCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

