// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "MythTypes.generated.h"

/**
 *  Game types library
 */


UENUM(BlueprintType)
enum class ECharacterSpeed : uint8
{
	Idle UMETA(DisplayName = "Idle speed"),
	Walk UMETA(DisplayName = "Walk speed"),
	Jog UMETA(DisplayName = "Jog speed"),
	Run UMETA(DisplayName = "Run speed"),
	Sprint UMETA(DisplayName = "Sprint speed")
};

UENUM(BlueprintType)
enum class EAIState : uint8
{
	Passive UMETA(DisplayName = "Passive state"),
	Patrol UMETA(DisplayName = "Patroling state"),
	Investigate UMETA(DisplayName = "Investigating state"),
	Search  UMETA(DisplayName = "Searching state"),
	Attack UMETA(DisplayName = "Attacking state"),
	Flee UMETA(DisplayName = "Fleeing state"),
	Stagger UMETA(DisplayName = "Staggered state"),
	Stun UMETA(DisplayName = "Stunned state"),
	Death UMETA(DisplayName = "Death state")
};

UENUM(BlueprintType)
enum class EAISense : uint8
{
	None UMETA(DisplayName = "AI sense None"),
	Sight UMETA(DisplayName = "AI sense Sight"),
	Hearing UMETA(DisplayName = "AI sense Hearing"),
	Damage UMETA(DisplayName = "AI sense Damage")
};

UENUM(BlueprintType)
enum class EDamageType : uint8
{
	None UMETA(DisplayName = "Damage type None"),
	Melee UMETA(DisplayName = "Damage type Melee"),
	Projectile UMETA(DisplayName = "Damage type Projectile"),
	Explosion UMETA(DisplayName = "Damage type Explosion"),
	Environment UMETA(DisplayName = "Damage type Environment")
};

UENUM(BlueprintType)
enum class EDamageResponse : uint8
{
	None UMETA(DisplayName = "Damage response None"),
	HitReaction UMETA(DisplayName = "Damage response HitReaction"),
	Stagger UMETA(DisplayName = "Damage response Stagger"),
	Stun UMETA(DisplayName = "Damage response Stun"),
	KnockBack UMETA(DisplayName = "Damage response KnockBack")	
};

USTRUCT(BlueprintType)
struct FDamageInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Info")
	float Amount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Info")
	EDamageType DamageType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Info")
	EDamageResponse DamageResponse;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Info")
	bool ShouldDamageInvincible;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Info")
	bool CanBeBlocked;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Info")
	bool CanBeParried;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Info")
	bool ShouldForceInterrupt;	
};
	


UCLASS()
class MYTH_API UMythTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
